# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostLoginTest do
  use ExUnit.Case

  test "POST login" do
    endpoint = %Polyjuice.Client.Endpoint.PostLogin{
      type: "m.login.password",
      identifier: %{
        "type" => "m.id.user",
        "user" => "alice"
      },
      password: "12345"
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert %{http_spec | body: nil} == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: false,
             body: nil,
             headers: [
               {"Accept", "application/json"},
               {"Accept-Encoding", "gzip, deflate"},
               {"Content-Type", "application/json"}
             ],
             method: :post,
             path: "_matrix/client/r0/login"
           }

    assert Jason.decode!(http_spec.body) == %{
             "type" => "m.login.password",
             "identifier" => %{
               "type" => "m.id.user",
               "user" => "alice"
             },
             "password" => "12345"
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             ~s({"user_id":"@alice:example.com","access_token":"1234567890","device_id":"ABCDEF","well_known":{"m.homeserver":{"base_url":"https://example.com"},"m.identity_server":{"base_url":"https://example.com"}}})
           ) == {
             :ok,
             %{
               "user_id" => "@alice:example.com",
               "access_token" => "1234567890",
               "device_id" => "ABCDEF",
               "well_known" => %{
                 "m.homeserver" => %{
                   "base_url" => "https://example.com"
                 },
                 "m.identity_server" => %{
                   "base_url" => "https://example.com"
                 }
               }
             }
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
