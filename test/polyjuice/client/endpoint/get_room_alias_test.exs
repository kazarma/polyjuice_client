# Copyright 2021 Arjan Scherpenisse <arjan@miraclethings.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetRoomAliasTest do
  use ExUnit.Case

  test "GET directory/room/{room_alias}" do
    endpoint = %Polyjuice.Client.Endpoint.GetRoomAlias{
      room_alias: "#room_alias"
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: false,
             method: :get,
             path: "_matrix/client/r0/directory/room/%23room_alias"
           } = TestUtil.http_spec_body_to_binary(http_spec)

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             "{\"room_id\": \"!foo1\", \"servers\": [\"example.com\"]}"
           ) == {:ok, {"!foo1", %{"servers" => ["example.com"]}}}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
