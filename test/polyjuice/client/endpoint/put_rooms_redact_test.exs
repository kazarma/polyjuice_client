# Copyright 2021 Ketsapiwiq <ketsapiwiq@protonmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PutRoomsRedactTest do
  use ExUnit.Case

  test "PUT rooms/{room_id}/redact/{event_id}/{txn_id}" do
    endpoint = %Polyjuice.Client.Endpoint.PutRoomsRedact{
      room: "!room_id",
      txn_id: "txn_id",
      event: "!redacted_event_id",
      reason: "Goodbye World!"
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert TestUtil.http_spec_body_to_binary(http_spec) == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: true,
             body: ~s({"reason":"Goodbye World!"}),
             headers: [
               {"Accept", "application/json"},
               {"Accept-Encoding", "gzip, deflate"},
               {"Content-Type", "application/json"}
             ],
             method: :put,
             path: "_matrix/client/r0/rooms/%21room_id/redact/%21redacted_event_id/txn_id"
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             "{\"event_id\": \"$foo1\"}"
           ) == {:ok, "$foo1"}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
