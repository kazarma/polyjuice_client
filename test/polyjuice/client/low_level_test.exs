# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.LowLevelTest do
  use ExUnit.Case

  test "transaction_id is unique" do
    client = Polyjuice.Client.LowLevel.new("http://localhost:8008")

    # the best that we can do is test that two calls to transaction_id return
    # different values
    assert Polyjuice.Client.API.transaction_id(client) !=
             Polyjuice.Client.API.transaction_id(client)
  end

  test "call" do
    {:ok, tmpdir} = TestUtil.mktmpdir("client-call-")

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.ClientTest.Httpd]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      client =
        Polyjuice.Client.LowLevel.new(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.ClientTest.Httpd/",
          access_token: "an_access_token",
          user_id: "@alice:example.org",
          test: true
        )

      # binary body
      assert Polyjuice.Client.API.call(
               client,
               %DummyEndpoint{
                 http_spec: %Polyjuice.Client.Endpoint.HttpSpec{
                   method: :put,
                   path: "foo",
                   headers: [],
                   body: "foobar",
                   auth_required: false
                 }
               }
             ) == {:ok, %{"foo" => "bar"}}

      # iolist body
      assert Polyjuice.Client.API.call(
               client,
               %DummyEndpoint{
                 http_spec: %Polyjuice.Client.Endpoint.HttpSpec{
                   method: :put,
                   path: "foo",
                   headers: [],
                   body: [?f, ["oo"], ["b", [?a | "r"]]],
                   auth_required: false
                 }
               }
             ) == {:ok, %{"foo" => "bar"}}

      :inets.stop(:httpd, httpd_pid)
    after
      File.rm_rf(tmpdir)
    end
  end

  test "login" do
    {:ok, tmpdir} = TestUtil.mktmpdir("login-")

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.ClientTest.Httpd]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      client =
        Polyjuice.Client.LowLevel.new(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.ClientTest.Httpd/",
          test: true
        )

      assert Polyjuice.Client.LowLevel.log_in_with_password(
               client,
               "@alice:example.org",
               "password"
             ) ==
               {:ok,
                %{
                  "access_token" => "m.id.user_login",
                  "device_id" => "foo",
                  "user_id" => "@alice:example.org"
                }}

      assert Polyjuice.Client.LowLevel.log_in_with_password(
               client,
               {:email, "user@example.com"},
               "password"
             ) ==
               {:ok,
                %{
                  "access_token" => "m.id.thirdparty_login",
                  "device_id" => "foo",
                  "user_id" => "@alice:example.org"
                }}

      assert Polyjuice.Client.LowLevel.log_in_with_password(
               client,
               {:phone, "CA", "1234567890"},
               "password"
             ) ==
               {:ok,
                %{
                  "access_token" => "m.id.phone_login",
                  "device_id" => "foo",
                  "user_id" => "@alice:example.org"
                }}

      assert Polyjuice.Client.LowLevel.log_in_with_password(
               client,
               %{
                 "type" => "ca.uhoreg.foo"
               },
               "password"
             ) ==
               {:ok,
                %{
                  "access_token" => "ca.uhoreg.foo_login",
                  "device_id" => "foo",
                  "user_id" => "@alice:example.org"
                }}

      :inets.stop(:httpd, httpd_pid)
    after
      File.rm_rf(tmpdir)
    end
  end

  test "register" do
    {:ok, tmpdir} = TestUtil.mktmpdir("register-")

    try do
      tmpdir_charlist = to_charlist(tmpdir)

      :inets.start()

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.ClientTest.Httpd]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      client =
        Polyjuice.Client.LowLevel.new(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.ClientTest.Httpd/",
          test: true
        )

      opts = [username: "alice", password: "password", device_id: "foo"]

      assert Polyjuice.Client.LowLevel.register(
               client,
               opts
             ) ==
               {:ok,
                %{
                  "access_token" => "aabbccddeeffgghh",
                  "device_id" => "foo",
                  "user_id" => "@alice:example.org"
                }}

      # Test error when registering if username is taken

      {:ok, httpd_pid} =
        :inets.start(
          :httpd,
          port: 0,
          server_name: 'sync.test',
          server_root: tmpdir_charlist,
          document_root: tmpdir_charlist,
          bind_address: {127, 0, 0, 1},
          modules: [:mod_esi],
          erl_script_alias: {'', [Polyjuice.ClientTest.Httpd.UserAlreadyTaken]}
        )

      port = :httpd.info(httpd_pid) |> Keyword.fetch!(:port)

      client =
        Polyjuice.Client.LowLevel.new(
          "http://127.0.0.1:#{port}/Elixir.Polyjuice.ClientTest.Httpd.UserAlreadyTaken/",
          test: true
        )

      assert {:error, 400, %{"errcode" => "M_USER_IN_USE", "error" => _}} =
               Polyjuice.Client.LowLevel.register(client, opts)

      :inets.stop(:httpd, httpd_pid)
    after
      File.rm_rf(tmpdir)
    end
  end
end
