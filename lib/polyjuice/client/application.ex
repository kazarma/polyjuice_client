# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Application do
  use Application

  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: Polyjuice.Client},
      %{
        id: Polyjuice.Client.ID,
        start: {Agent, :start_link, [fn -> 0 end, [name: Polyjuice.Client.ID]]}
      },
      {Mutex, name: Polyjuice.Client.Mutex}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
