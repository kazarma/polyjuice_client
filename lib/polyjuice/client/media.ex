# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Media do
  @moduledoc """
  Media-related functions.
  """
  require Logger

  @doc """
  Upload a file to the media repository.

  `data` may be either a binary, indicating the file contents, or a tuple
  `{:file, path}`, where `path` is a path to the file to be uploaded.

  `opts` is a keyword list of options.  Recognized options are:
  - `filename:` the filename to use for the uploaded file.  This is required
    when `data` is a binary.  If not specified, and `data` is of the form
    `{:file, path}`, then the filename defaults to the basename of the path.
  - `mimetype:` the mimetype to use for the uploaded file.  Defaults to
    `application/octet-stream`.
  """
  @spec upload(
          client_api :: Polyjuice.Client.API.t(),
          data :: binary | {:file, String.t()},
          opts :: Keyword.t()
        ) :: {:ok, String.t()} | any
  def upload(client_api, data, opts \\ []) do
    filename =
      Keyword.get_lazy(opts, :filename, fn ->
        case data do
          {:file, name} ->
            Path.basename(name)
        end
      end)

    mimetype = Keyword.get(opts, :mimetype, "application/octet-stream")

    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.PostMediaUpload{
        filename: filename,
        data: data,
        mimetype: mimetype
      }
    )
  end

  @doc """
  Download a file from the media repository.

  `url` may be either a binary or a `URI` (giving an `mxc://` URI to download),
  or a `{server_name, media_id}` tuple.  `filename` is an (optional) filename to
  request that the server use, and `allow_remote` indicates whether the server
  should fetch media from remote servers if necessary (defaults to true).

  If successful, returns a tuple of the form `{:ok, filename, content_type, body}`,
  where `body` is a `Stream` such that `Enum.join(body)` is the file contents.
  """
  @spec download(
          client_api :: Polyjuice.Client.API.t(),
          url :: String.t() | URI.t() | {String.t(), String.t()},
          filename :: String.t() | nil | boolean,
          allow_remote :: boolean
        ) :: {:ok, String.t(), String.t(), Enumerable.t()} | any
  def download(client_api, url, filename \\ nil, allow_remote \\ true)
      when is_boolean(allow_remote) do
    # handle the case where filename is omitted, but allow_remote is provided,
    # which means that "filename" will actually be allow_remote, and filename
    # should be nil
    {filename, allow_remote} =
      cond do
        is_boolean(filename) -> {nil, filename}
        true -> {filename, allow_remote}
      end

    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.GetMediaDownload{
        url: url,
        allow_remote: allow_remote,
        filename: filename
      }
    )
  end

  # 13.8.2.4   GET /_matrix/media/r0/thumbnail/{serverName}/{mediaId}
  # 13.8.2.5   GET /_matrix/media/r0/preview_url
  # 13.8.2.6   GET /_matrix/media/r0/config
end
