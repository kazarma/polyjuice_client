# Copyright 2020 Multi Prise <multiprisestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Profile do
  @moduledoc """
  Profile-related functions.
  """
  require Logger

  @doc """
  Get user profile.

  `user_id` is the ID of the user whose profile should be retrieved.  If
  `user_id` is ommited it defaults to the user_id represented by the client.
  """
  @spec get_profile(
          client_api :: Polyjuice.Client.API.t(),
          user_id :: String.t() | nil
        ) :: {:ok, map()} | any
  def get_profile(client_api, user_id \\ nil)
      when is_binary(user_id) or user_id == nil do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.GetProfile{
        user_id: user_id || Polyjuice.Client.API.get_user_and_device(client_api) |> elem(0)
      }
    )
  end

  @doc """
  Get avatar url for the user.

  `user_id` is the ID of user whose profile should be retrieved.  If
  `user_id` is ommited it defaults to the user_id represented by the client.
  """
  @spec get_avatar_url(
          client_api :: Polyjuice.Client.API.t(),
          user_id :: String.t() | nil
        ) :: {:ok, String.t()} | any
  def get_avatar_url(client_api, user_id \\ nil)
      when is_binary(user_id) or user_id == nil do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.GetProfileAvatarUrl{
        user_id: user_id || Polyjuice.Client.API.get_user_and_device(client_api) |> elem(0)
      }
    )
  end

  @doc """
  Get display name for the user.

  `user_id` is the ID of the user whose profile should be retrieved.  If
  `user_id` is ommited it defaults to the user_id represented by the client.
  """
  @spec get_displayname(
          client_api :: Polyjuice.Client.API.t(),
          user_id :: String.t() | nil
        ) :: {:ok, String.t()} | any
  def get_displayname(client_api, user_id \\ nil)
      when is_binary(user_id) or user_id == nil do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.GetProfileDisplayname{
        user_id: user_id || Polyjuice.Client.API.get_user_and_device(client_api) |> elem(0)
      }
    )
  end

  @doc """
  Modify avatar url for the user.

  `user_id` is the ID of the user whose profile should be retrieved.  If
  `user_id` is ommited it defaults to the user_id represented by the client.
  `avatar_url` is an `mxc:` URL specifying the location of the new profile
  avatar.
  """
  @spec put_avatar_url(
          client_api :: Polyjuice.Client.API.t(),
          user_id :: String.t() | nil,
          avatar_url :: String.t()
        ) :: :ok | any
  def put_avatar_url(client_api, user_id \\ nil, avatar_url)
      when is_binary(user_id) or user_id == nil do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.PutProfileAvatarUrl{
        user_id: user_id || Polyjuice.Client.API.get_user_and_device(client_api) |> elem(0),
        avatar_url: avatar_url
      }
    )
  end

  @doc """
  Modify display name for the user.

  `user_id` is the ID of the user whose profile should be retrieved.  If
  `user_id` is ommited it defaults to the user_id represented by the client.
  `displayname` is the new displayname.
  """
  @spec put_displayname(
          client_api :: Polyjuice.Client.API.t(),
          user_id :: String.t() | nil,
          displayname :: String.t()
        ) :: :ok | any
  def put_displayname(client_api, user_id \\ nil, displayname)
      when is_binary(user_id) or user_id == nil do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.PutProfileDisplayname{
        user_id: user_id || Polyjuice.Client.API.get_user_and_device(client_api) |> elem(0),
        displayname: displayname
      }
    )
  end
end
