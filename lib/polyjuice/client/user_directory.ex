# Copyright 2021 Ketsapiwiq <ketsapiwiq@protonmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.UserDirectory do
  @moduledoc """
  User directory related functions.

  """
  require Logger

  @doc """
  Search for a user.

  `search_term` can be any string.
  `limit` is the results limit, unlimited if not specified or `nil`.


  Examples:

      iex> Polyjuice.Client.UserDirectory.search_user(client, "username")
      {:ok,
      %{
        "limited" => false,
        "results" => [
          %{
            "avatar_url" => nil,
            "display_name" => "username",
            "user_id" => "@username:kazarma.local"
          }
        ]
      }}

      iex> Polyjuice.Client.UserDirectory.search_user(
      ...>  client,
      ...>  "username",
      ...>  10)
      {:ok,
      %{
        "limited" => false,
        "results" => [
          %{
            "avatar_url" => nil,
            "display_name" => "username",
            "user_id" => "@username:kazarma.local"
          }
        ]
      }}

  """
  @spec search_user(
          client_api :: Polyjuice.Client.API.t(),
          search_term :: String.t(),
          limit :: integer | nil
        ) :: {:ok, map()} | any
  def search_user(client_api, search_term, limit \\ nil)
      when is_binary(search_term) and (is_integer(limit) or limit == nil) do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.PostUserDirectorySearch{
        search_term: search_term,
        limit: limit
      }
    )
  end
end
