# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostRoomsForget do
  @moduledoc """
  Leave a room.

  https://matrix.org/docs/spec/client_server/latest#post-matrix-client-r0-rooms-roomid-forget
  """

  @type t :: %__MODULE__{
          room: String.t()
        }

  @enforce_keys [:room]
  defstruct [
    :room
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%{room: room}) do
      e = &URI.encode_www_form/1

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "rooms/#{e.(room)}/forget",
        body: "{}"
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, _parsed) do
      :ok
    end
  end
end
