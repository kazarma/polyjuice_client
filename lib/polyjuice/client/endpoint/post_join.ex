# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostJoin do
  @moduledoc """
  Join a room.

  https://matrix.org/docs/spec/client_server/r0.5.0#post-matrix-client-r0-join-roomidoralias
  """

  @type t :: %__MODULE__{
          room: String.t(),
          servers: [String.t()],
          third_party_signed: map | nil
        }

  @enforce_keys [:room]
  defstruct [
    :room,
    :third_party_signed,
    servers: []
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%{
          room: room,
          servers: servers,
          third_party_signed: third_party_signed
        }) do
      e = &URI.encode_www_form/1

      body =
        Jason.encode_to_iodata!(
          if third_party_signed do
            %{"third_party_signed" => third_party_signed}
          else
            %{}
          end
        )

      query =
        if servers == [] do
          nil
        else
          servers |> Enum.map(&{:server_name, &1})
        end

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "join/#{e.(room)}",
        query: query,
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, parsed) do
      {:ok, Map.get(parsed, "room_id")}
    end
  end
end
